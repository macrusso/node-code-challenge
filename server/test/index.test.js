import "regenerator-runtime/runtime.js";
import request from 'supertest';
import { setDb, db } from '../src/data/db.js';
import { app, server } from '../src/index.js';
import response from './response.json';

beforeAll(() => {
    process.env.DATABASE = './test/locations.test.db';
    setDb();
});

test('get locations', async () => {
    const res = await request(app).get('/locations?q=oxford');
    expect(res.status).toBe(200);
    expect(res.body).toEqual(response);
});

test('get locations missing query param', async () => {
    const res = await request(app).get('/locations');
    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: 'Query parameter missing' });
});

test('get locations query shorter than 2 chars', async () => {
    const res = await request(app).get('/locations?q=o');
    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: 'Query should be 2 or more letters' });
});

afterAll(() => {
    server.close();
});