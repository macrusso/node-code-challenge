import sqlite3 from 'sqlite3';

export let db;

export function setDb() {
    sqlite3.verbose();

    db = new sqlite3.Database(process.env.DATABASE, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the locations database.');
    });

    db.serialize(() => {
        db.run("DROP TABLE IF EXISTS locationsV;");
        db.run("CREATE VIRTUAL TABLE locationsV USING fts4(name, latitude, longitude);");
        db.run(`INSERT INTO locationsV(name, latitude, longitude)
                SELECT name, latitude, longitude
                FROM locations;
            `);
    });
}