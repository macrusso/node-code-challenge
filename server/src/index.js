import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import cors from 'cors';
import { setDb } from './data/db.js';
import locationsRoutes from './routes/locations.js';

dotenv.config();

const PORT = process.env.PORT || 5000;
const app = express();
setDb('./src/data/locations.db');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use('/locations', locationsRoutes);

const server = app.listen(PORT, () => {
    console.log(`Server listening at http://localhost:${PORT}`);
});

export { app, server };