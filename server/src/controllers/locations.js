import { db } from '../data/db.js';

export const getLocations = (req, res) => {
    const { q: query } = req.query;
    if (!query) {
        return res.status(400).json({ error: 'Query parameter missing' });
    }

    if (query.length < 2) {
        return res.status(400).json({ error: 'Query should be 2 or more letters' });
    }

    db.all("SELECT * FROM locationsV WHERE name MATCH ?", `*${query}*`, (err, rows) => {
        if (err) {
            console.error(err.message);
            return res.status(500).json({ error: 'Server error' });
        }

        return res.json({
            locationsCount: rows.length,
            data: rows
        });
    });
};
