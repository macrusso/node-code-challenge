export default {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  coverageProvider: "babel",
  transform: {
    "^.+\\.jsx?$": "babel-jest"
  },
};
