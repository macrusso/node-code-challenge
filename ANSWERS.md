# Questions

Qs1: 
```js
// 2
// 1
```
`2` goes first as there is no delay. `1` will be delayed by 100 ms with `setTimeout`

Qs2: 
```js
// 10
// 9
// 8
// 7
// 6
// 5
// 4
// 3
// 2
// 1
// 0
```
Recursive function will be looped on `if` statement until it'll reach `d >= 10`. Then loop will finish and I'll go to `console.log` and log all values starting with the latest one. 

Qs3: The code will work only for *falsy* values. It will fail on *truthy* values like `{}`, `[]` or `'0'`.

Qs4: That's a closure. Where function `foo` will create a new function (factory) with variable `a`. That new function will take another argument and add them up. 

```js
// 3
```

Qs5: That's a function with callback with delayed call to the function. Function would double the number provided.  
